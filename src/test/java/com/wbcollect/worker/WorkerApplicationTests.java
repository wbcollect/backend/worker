package com.wbcollect.worker;

import com.google.protobuf.Timestamp;
import com.wbcollect.bot.BotProto;
import com.wbcollect.initializers.KafkaInitializer;
import com.wbcollect.initializers.KafkaSASLInitializer;
import com.wbcollect.initializers.PostgresInitializer;
import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import com.wbcollect.worker.grpc.WorkerGrpcService;
import com.wbcollect.worker.publisher.TaskPublisher;
import com.wbcollect.worker.repository.ProductDataRepository;
import com.wbcollect.worker.repository.ProductDataTsRepository;
import io.grpc.internal.testing.StreamRecorder;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@Testcontainers
@SpringBootTest
@DirtiesContext
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        KafkaInitializer.class,
        PostgresInitializer.class,
})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class WorkerApplicationTests {
    @Container
    private static final PostgreSQLContainer POSTGRE_SQL_CONTAINER = PostgresInitializer.POSTGRE_SQL_CONTAINER;

    @Container
    private static final KafkaContainer KAFKA_CONTAINER = KafkaSASLInitializer.KAFKA_CONTAINER;

    @Autowired
    public KafkaTemplate<String, byte[]> kafkaTemplate;

    @Autowired
    public ConsumerFactory<String, byte[]> consumerFactory;

    @Autowired
    public ProductDataRepository productDataRepository;

    @Autowired
    public ProductDataTsRepository productDataTsRepository;

    @Autowired
    private WorkerGrpcService workerGrpcService;

    @Autowired
    private Map<String, Integer> kafkaOffsets;

    @Autowired
    private TaskPublisher taskPublisher;

    @Value("classpath:test-product-response-1.json")
    Resource productResponse1;

    @Value("classpath:test-product-response-2.json")
    Resource productResponse2;

    @Value("classpath:test-category-page-response.json")
    Resource categoryResponse1;

    @Value("classpath:menu.json")
    Resource menuResponse;

    @BeforeEach
    public void setUp() {
        kafkaTemplate.setConsumerFactory(consumerFactory);
    }

    @TestConfiguration
    static class Config {
        @Bean
        public Map<String, Integer> kafkaOffsets() {
            return new HashMap<>();
        }
    }

    @Test
    public void receiveTwoTasksFromInboundTopic_successfullyReturnChangingInTimeStats() throws Exception {
        taskPublisher.publishFirstCategoryPageDataDownloadTask("/catalog/dom/dachniy-sezon/sadovaya-tehnika/sadovye-izmelchiteli");
        taskPublisher.publishProductDataDownloadTask(12515574L);
        taskPublisher.publishProductDataDownloadTask(12515574L);
        ;
        receiveKafkaMessageAndRespondWith(StreamUtils.copyToString(categoryResponse1.getInputStream(), StandardCharsets.UTF_8));

        Thread.sleep(500);

        Instant startTime = Instant.now();

        receiveKafkaMessageAndRespondWith(StreamUtils.copyToString(productResponse1.getInputStream(), StandardCharsets.UTF_8));

        Thread.sleep(100);

        // check results
        var optionalProductData = productDataRepository.findById(12515574L);
        var allProductDataTs = productDataTsRepository.findAll();
        assertFalse(optionalProductData.isEmpty() || allProductDataTs.isEmpty());

        var productData = optionalProductData.get();
        assertEquals(12515574, productData.getProductId().intValue());
        assertEquals("Измильчитель электрический SE26", productData.getName());
        assertEquals(5, productData.getRating().intValue());
        assertEquals(6, productData.getFeedbackCount().intValue());

        var productDataTs = allProductDataTs.get(0);
        assertEquals(21238, productDataTs.getPrice().intValue());
        assertEquals(161, productDataTs.getQuantity().intValue());


        /* SECOND CHECK */
        receiveKafkaMessageAndRespondWith(StreamUtils.copyToString(productResponse2.getInputStream(), StandardCharsets.UTF_8));

        Thread.sleep(100);

        // check results
        optionalProductData = productDataRepository.findById(12515574L);
        allProductDataTs = productDataTsRepository.findAll();
        assertFalse(optionalProductData.isEmpty() && allProductDataTs.isEmpty());

        productData = optionalProductData.get();
        assertEquals(12515574, productData.getProductId().intValue());
        // check if field name is updated (from "Измильчитель" to "Измельчитель")
        assertEquals("Измельчитель электрический SE26", productData.getName());
        assertEquals(5, productData.getRating().intValue());
        assertEquals(6, productData.getFeedbackCount().intValue());

        assertEquals(2, allProductDataTs.size());
        productDataTs = allProductDataTs.get(1);
        assertEquals(42, productDataTs.getPrice().intValue());
        assertEquals(365, productDataTs.getQuantity().intValue());

        /*
         * GRPC TEST
         * */
        Instant endTime = Instant.now();
        var requestFromStatisticService = WorkerProto.ProductDataTsRequest
                .newBuilder()
                .setStartDate(Timestamp.newBuilder()
                        .setSeconds(startTime.getEpochSecond())
                        .setNanos(startTime.getNano())
                        .build())
                .setEndDate(Timestamp.newBuilder()
                        .setSeconds(endTime.getEpochSecond())
                        .setNanos(endTime.getNano())
                        .build())
                .build();
        StreamRecorder<WorkerProto.ProductDataTsReply> responseObserver = StreamRecorder.create();
        workerGrpcService.getProductDataTs(requestFromStatisticService, responseObserver);
        if (!responseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            fail("The call did not terminate in time");
        }
        assertNull(responseObserver.getError());
        List<WorkerProto.ProductDataTsReply> results = responseObserver.getValues();
        assertEquals(1, results.size());
        var response = results.get(0).getProductDataTsArrayList().get(1);
        assertEquals(12515574, response.getProductId());
        assertEquals(42, response.getPrice());
        assertEquals(365, response.getQuantity());
        assertEquals("/dom/dachniy-sezon/sadovaya-tehnika/sadovye-izmelchiteli", response.getCategoryUrl());
    }

    @Test
    public void receiveMenuJsonTask_checkIfAllCategoriesAddedToKafka() throws IOException, InterruptedException {
        taskPublisher.publishMenuBurgerDownloadTask();
        receiveKafkaMessageAndRespondWith(StreamUtils.copyToString(menuResponse.getInputStream(), StandardCharsets.UTF_8));

        Thread.sleep(500);
        int tasksQueued = receiveAndCountAllMessagesInQueue();
        assertEquals(1685, tasksQueued);
    }

    @Test
    public void receiveCategoryPageJsonTask_checkIfAllProductsAddedToKafka() throws InterruptedException, IOException {
        taskPublisher.publishFirstCategoryPageDataDownloadTask("/catalog/dom/dachniy-sezon/sadovaya-tehnika/sadovye-izmelchiteli");

        receiveKafkaMessageAndRespondWith(StreamUtils.copyToString(categoryResponse1.getInputStream(), StandardCharsets.UTF_8));

        Thread.sleep(100);
        int tasksQueued = receiveAndCountAllMessagesInQueue();

        // 100 products + next category page
        assertEquals(101, tasksQueued);
        assertEquals(100, productDataRepository.findAll().size());
    }


    private void receiveKafkaMessageAndRespondWith(String response) {
        var newTask = receiveMessage(Topics.BOT.INBOUND_TASK_NAME);
        assertNotNull(newTask);
        var inboundTask = ProtoUtils.parseOrThrow(newTask.value(), Topics.BOT.INBOUND_TASK_CLASS);
        var outboundTask = BotProto.OutboundTask.newBuilder()
                .setId(inboundTask.getId())
                .setUrl(inboundTask.getUrl())
                .setData(response)
                .setStatus(200)
                .build();
        kafkaTemplate.send(Topics.BOT.OUTBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(outboundTask));
    }

    private ConsumerRecord<String, byte[]> receiveMessage(String topic) {
        int offset = kafkaOffsets.getOrDefault(topic, 0);
        ConsumerRecord<String, byte[]> result = kafkaTemplate.receive(topic, 0, offset);
        if (result != null) {
            kafkaOffsets.put(topic, ++offset);
        }
        return result;
    }

    private int receiveAndCountAllMessagesInQueue() {
        int receivedTasksCounter = 0;
        while (true) {
            var kafkaMessage = receiveMessage(Topics.BOT.INBOUND_TASK_NAME);
            if (kafkaMessage == null)
                break;
            var outboundTask = BotProto.OutboundTask.newBuilder()
                    .setStatus(400)
                    .build();
            receivedTasksCounter++;
            kafkaTemplate.send(Topics.BOT.OUTBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(outboundTask));
        }
        return receivedTasksCounter;
    }
}
