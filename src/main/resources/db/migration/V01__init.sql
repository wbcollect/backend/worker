CREATE TABLE IF NOT EXISTS product_data
(
    product_id BIGINT NOT NULL,
    brand_id BIGINT,
    brand_name VARCHAR(255),
    category_url VARCHAR(255),
    feedback_count INTEGER,
    name VARCHAR(255),
    rating INTEGER,
    PRIMARY KEY (product_id)
);

CREATE TABLE IF NOT EXISTS product_data_ts
(
    product_id BIGINT NOT NULL,
    taken_in TIMESTAMP(6) NOT NULL,
    price INTEGER,
    price_with_sale INTEGER,
    quantity INTEGER,
    PRIMARY KEY (product_id, taken_in)
);
