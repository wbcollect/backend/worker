-- Rename the "taken_in" field to "taken_at" in the "product_data_ts" table
-- Remove "category_url" column

-- Create a new temporary column "taken_at_copy"
ALTER TABLE product_data_ts ADD COLUMN taken_at_copy TIMESTAMP(6);

-- Copy the data from "taken_in" to "taken_at_copy"
UPDATE product_data_ts SET taken_at_copy = taken_in;

-- Drop the "taken_in" and "category_url" columns
ALTER TABLE product_data_ts DROP COLUMN taken_in;
ALTER TABLE product_data DROP COLUMN category_url;

-- Rename the "taken_at_copy" column to "taken_at"
ALTER TABLE product_data_ts RENAME COLUMN taken_at_copy TO taken_at;