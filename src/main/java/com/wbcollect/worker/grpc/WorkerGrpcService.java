package com.wbcollect.worker.grpc;

import com.google.protobuf.Timestamp;
import com.wbcollect.worker.WorkerGrpc;
import com.wbcollect.worker.WorkerProto;
import com.wbcollect.worker.entity.ProductData;
import com.wbcollect.worker.entity.ProductDataTs;
import com.wbcollect.worker.repository.ProductDataTsRepository;
import com.wbcollect.worker.repository.ProductDataRepository;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@net.devh.boot.grpc.server.service.GrpcService
public class WorkerGrpcService extends WorkerGrpc.WorkerImplBase {
    private final ProductDataTsRepository productDataTsRepository;
    private final ProductDataRepository productDataRepository;

    @Override
    public void getProductDataTs(WorkerProto.ProductDataTsRequest request, StreamObserver<WorkerProto.ProductDataTsReply> responseObserver) {
        var startDate = Date.from(Instant.ofEpochSecond(request.getStartDate().getSeconds(), request.getStartDate().getNanos()));
        var endDate = Date.from(Instant.ofEpochSecond(request.getEndDate().getSeconds(), request.getEndDate().getNanos()));
        log.info("Received request to send new ProductDataTs in range: " + startDate + " - " + endDate);
        responseObserver.onNext(
                WorkerProto.ProductDataTsReply
                        .newBuilder()
                        .addAllProductDataTsArray(getProductDataTsArrayInRange(startDate, endDate))
                        .build()
        );
        responseObserver.onCompleted();
    }

    private List<WorkerProto.ProductDataTs> getProductDataTsArrayInRange(Date startDate, Date endDate) {
        List<WorkerProto.ProductDataTs> newProductDataTsArray = new ArrayList<>();
        for (ProductDataTs productDataTs : productDataTsRepository.findAllByTakenAtBetween(startDate, endDate)) {
            newProductDataTsArray.add(
                    mapProductDataTs(productDataTs,
                            productDataRepository.findById(productDataTs.getProductId()).orElseThrow()));
        }
        return newProductDataTsArray;
    }

    private static WorkerProto.ProductDataTs mapProductDataTs(ProductDataTs productDataTs, ProductData productData) {
        var takenAt = productDataTs.getTakenAt().toInstant();

        return WorkerProto.ProductDataTs.newBuilder()
                .setProductId(productDataTs.getProductId())
                .setTakenAt(Timestamp.newBuilder()
                        .setSeconds(takenAt.getEpochSecond())
                        .setNanos(takenAt.getNano())
                        .build())
                .setName(productData.getName())
                .setBrand(productData.getBrandName())
                .setCategoryUrl(productData.getCategoryUrl())
                //.setSeller(productData.getSeller())
                .setPrice(productDataTs.getPrice())
                .setPriceWithSale(productDataTs.getPriceWithSale())
                .setQuantity(productDataTs.getQuantity())
                .build();
    }
}
