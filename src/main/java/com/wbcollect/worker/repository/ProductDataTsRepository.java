package com.wbcollect.worker.repository;

import com.wbcollect.worker.entity.ProductDataTs;
import com.wbcollect.worker.entity.ProductDataTsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProductDataTsRepository extends JpaRepository<ProductDataTs, ProductDataTsId> {
    List<ProductDataTs> findAllByTakenAtBetween(Date start, Date end);
}
