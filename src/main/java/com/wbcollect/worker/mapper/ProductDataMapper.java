package com.wbcollect.worker.mapper;

import com.wbcollect.worker.entity.ProductData;
import com.wbcollect.worker.model.wildberries.product.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductDataMapper {
    ProductDataMapper INSTANCE = Mappers.getMapper(ProductDataMapper.class);
    @Mapping(target="productId", source="product.id")
    @Mapping(target="feedbackCount", source="product.feedbacks")
    @Mapping(target="brandName", source="product.brand")
    ProductData productToProductData(Product product);
}
