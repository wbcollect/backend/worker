package com.wbcollect.worker.model.wildberries.product;

import java.util.ArrayList;

public class Product {
    public int id;
    public int root;
    public int kindId;
    public int subjectId;
    public int subjectParentId;
    public String name;
    public String brand;
    public int brandId;
    public int siteBrandId;
    public int supplierId;
    public int sale;
    public int priceU;
    public int salePriceU;
    public int logisticsCost;
    public Extended extended;
    public int saleConditions;
    public int pics;
    public int rating;
    public int feedbacks;
    public int volume;
    public ArrayList<Object> colors;
    public ArrayList<Integer> promotions;
    public ArrayList<Size> sizes;
    public boolean diffPrice;
    public int time1;
    public int time2;
    public int wh;
}
