package com.wbcollect.worker.model.wildberries.product;

import java.util.ArrayList;

public class Size {
    public String name;
    public String origName;
    public int rank;
    public int optionId;
    public ArrayList<Stock> stocks;
    public int time1;
    public int time2;
    public int wh;
    public String sign;
}
