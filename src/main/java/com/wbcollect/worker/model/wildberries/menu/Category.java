package com.wbcollect.worker.model.wildberries.menu;

import java.util.ArrayList;

public class Category {
    public int id;
    public int parent;
    public String name;
    public String seo;
    public String url;
    public String shard;
    public String query;
    public ArrayList<Category> childs;
    public boolean isDenyLink;
    public ArrayList<Integer> dest;
}
