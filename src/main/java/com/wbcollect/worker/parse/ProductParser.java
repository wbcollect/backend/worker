package com.wbcollect.worker.parse;

import com.google.gson.Gson;
import com.wbcollect.worker.entity.ProductData;
import com.wbcollect.worker.entity.ProductDataTs;
import com.wbcollect.worker.mapper.ProductDataMapper;
import com.wbcollect.worker.model.wildberries.product.Root;
import com.wbcollect.worker.model.wildberries.product.Size;
import com.wbcollect.worker.model.wildberries.product.Stock;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class ProductParser {
    private final Gson gson;
    private final ProductDataMapper productDataMapper = Mappers.getMapper(ProductDataMapper.class);


    public ProductData getProductInfo(String json) {
        var product = gson.fromJson(json, Root.class).data.products.get(0);

        return productDataMapper.productToProductData(product);
    }

    public ProductDataTs getProductStatisticInfo(String json) {
        var root = gson.fromJson(json, Root.class);
        var productData = root.data.products.get(0);
        int quantity = countAllStocks(productData.sizes);

        return ProductDataTs.builder()
                .productId((long) productData.id)
                .takenAt(new Date())
                .price(productData.priceU / 100)
                .priceWithSale(productData.salePriceU / 100)
                .quantity(quantity)
                .build();
    }

    private int countAllStocks(ArrayList<Size> sizes) {
        int stocksCounter = 0;
        for (Size size : sizes) {
            for (Stock stock : size.stocks) {
                stocksCounter += stock.qty;
            }
        }
        return stocksCounter;
    }
}
