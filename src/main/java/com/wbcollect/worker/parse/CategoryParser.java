package com.wbcollect.worker.parse;

import com.google.gson.Gson;
import com.wbcollect.worker.entity.ProductData;
import com.wbcollect.worker.model.wildberries.category.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryParser {
    private final Gson gson;

    public List<ProductData> parseAllProductsFromCategoryPage(String json, String categoryUrl) {
        Root root = gson.fromJson(json, Root.class);
        List<ProductData> productDataList = new ArrayList<>();
        if (root.data.products == null)
            return productDataList;
        for (Integer productId : root.data.products) {
            productDataList.add(
                    ProductData.builder()
                            .productId(Long.valueOf(productId))
                            .categoryUrl(cropCategoryUrl(categoryUrl))
                            .build()
            );
        }
        return productDataList;
    }

    private String cropCategoryUrl(String categoryUrl) {
        StringBuilder sb = new StringBuilder();
        var parts = categoryUrl.split("/+|\\?");
        for (int i = 4; i < parts.length - 1; i++) {
            sb.append('/');
            sb.append(parts[i]);
        }
        return sb.toString();
    }
}
