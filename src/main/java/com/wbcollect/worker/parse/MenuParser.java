package com.wbcollect.worker.parse;

import com.google.gson.Gson;
import com.wbcollect.worker.model.wildberries.menu.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MenuParser {
    private final Gson gson;

    public List<Category> parseAllCategoriesFromMenuJson(String json) {
        Category[] children = gson.fromJson(json, Category[].class);
        List<Category> categories = new ArrayList<>();
        for (Category category : children) {
            saveAllCategoriesFromMenuTree(category, categories);
        }

        return categories;
    }

    private void saveAllCategoriesFromMenuTree(Category category, List<Category> categories) {
        if (category == null)
            return;
        if (category.childs == null) {
            if (category.url.startsWith("/catalog"))
                categories.add(category);
            return;
        }
        for (Category subcategory : category.childs) {
            saveAllCategoriesFromMenuTree(subcategory, categories);
        }
    }
}
