package com.wbcollect.worker.service;

import com.wbcollect.bot.BotProto;
import com.wbcollect.worker.entity.ProductData;
import com.wbcollect.worker.model.wildberries.menu.Category;
import com.wbcollect.worker.parse.CategoryParser;
import com.wbcollect.worker.parse.MenuParser;
import com.wbcollect.worker.parse.ProductParser;
import com.wbcollect.worker.publisher.TaskPublisher;
import com.wbcollect.worker.repository.ProductDataRepository;
import com.wbcollect.worker.repository.ProductDataTsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WorkerService {
    private final ProductParser productParser;
    private final CategoryParser categoryParser;
    private final MenuParser menuParser;
    private final ProductDataRepository productDataRepository;
    private final ProductDataTsRepository productDataTsRepository;
    private final TaskPublisher taskPublisher;

    public void parseAndSaveReceivedData(BotProto.OutboundTask outboundTask) {
        if (outboundTask.getStatus() == 200) {
            // if it's product info json
            if (outboundTask.getUrl().contains("detail")) {
                // create productData if not exists, or update fields if changed
                var productData = productParser.getProductInfo(outboundTask.getData());
                var optionalProductData = productDataRepository.findById(productData.getProductId());
                if (optionalProductData.isPresent()) {
                    var productDataEntity = optionalProductData.get();
                    productData.setCategoryUrl(productDataEntity.getCategoryUrl());
                }
                productDataRepository.save(productData);
                // insert productDataTs with timestamp and count available
                productDataTsRepository.save(productParser.getProductStatisticInfo(outboundTask.getData()));
            // if it's category info json
            } else if (outboundTask.getUrl().contains("catalog")) {
                var productDataList =
                        categoryParser.parseAllProductsFromCategoryPage(outboundTask.getData(), outboundTask.getUrl());
                productDataRepository.saveAll(productDataList);

                for (ProductData productData : productDataList) {
                    taskPublisher.publishProductDataDownloadTask(productData.getProductId());
                }
                // if this page had products, then check next
                if (!productDataList.isEmpty())
                    taskPublisher.publishNextCategoryPageDataDownloadTask(outboundTask.getUrl());
            }
            // if it's menu json
            else {
                var categoriesList = menuParser.parseAllCategoriesFromMenuJson(outboundTask.getData());

                for (Category category : categoriesList) {
                    taskPublisher.publishFirstCategoryPageDataDownloadTask(category.url);
                }
            }

        } else {
            log.error("Received completed task " + outboundTask.getUrl()
                    + " with error code " + outboundTask.getStatus() + ", body: " + outboundTask.getData());
        }
    }
}
