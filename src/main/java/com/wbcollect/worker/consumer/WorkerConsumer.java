package com.wbcollect.worker.consumer;

import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import com.wbcollect.worker.service.WorkerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static com.wbcollect.common.kafka.KafkaConst.DLQ_ERROR_HANDLER;
import static com.wbcollect.common.kafka.KafkaConst.SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY;

@Slf4j
@Component
@RequiredArgsConstructor
public class WorkerConsumer {
    private final WorkerService workerService;

    @KafkaListener(
            topics = Topics.BOT.OUTBOUND_TASK_NAME,
            containerFactory = SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY,
            errorHandler = DLQ_ERROR_HANDLER
    )
    public void onOutboundTask(@Payload final byte[] payload) {
        var outboundTask = ProtoUtils.parseOrThrow(payload, Topics.BOT.OUTBOUND_TASK_CLASS);
        log.info("Received completed task, id: " + outboundTask.getId() + ", url: " + outboundTask.getUrl());
        workerService.parseAndSaveReceivedData(outboundTask);
    }
}
