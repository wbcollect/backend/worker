package com.wbcollect.worker.publisher;

import com.wbcollect.bot.BotProto;
import com.wbcollect.proto.Topics;
import com.wbcollect.proto.util.ProtoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
@RequiredArgsConstructor
public class TaskPublisher {
    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    private static final String wbApiPrefix = "https://napi.wildberries.ru/api";
    private static final String wbApiProductUrlPrefix = "https://card.wb.ru/cards/detail?dest=-1138534&nm=";
    private static final String wbApiGetMenu = "https://static-basket-01.wb.ru/vol0/data/main-menu-ru-ru-v2.json";

    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");


    public void publishProductDataDownloadTask(Long productId) {
        publish(wbApiProductUrlPrefix + productId);
        log.info("Task for product data download queued, product id: " + productId);
    }

    public void publishNextCategoryPageDataDownloadTask(String previousUrl) {
        String nextUrl = increment(previousUrl);
        publish(nextUrl);
        log.info("Task for category page download queued, url: " + nextUrl);
    }

    public void publishFirstCategoryPageDataDownloadTask(String categoryUrl) {
        publish(wbApiPrefix + categoryUrl + "?page=1");
        log.info("Task for category " + categoryUrl + " download queued");
    }

    public void publishMenuBurgerDownloadTask() {
        publish(wbApiGetMenu);
        log.info("Daily task for menu download is queued");
    }

    private void publish(String taskUrl) {
        var payload = BotProto.InboundTask.newBuilder()
                .setId(System.currentTimeMillis())
                .setUrl(taskUrl)
                .build();
        kafkaTemplate.send(Topics.BOT.INBOUND_TASK_NAME, ProtoUtils.toByteArrayDelimitedOrThrow(payload));
    }

    private static String increment(String previousUrl) {
        Matcher m = NUMBER_PATTERN.matcher(previousUrl);
        if (!m.find())
            throw new NumberFormatException();
        String num = m.group();
        int inc = Integer.parseInt(num) + 1;
        String incStr = String.format("%0" + num.length() + "d", inc);

        return  m.replaceFirst(incStr);
    }
}
