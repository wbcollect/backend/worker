package com.wbcollect.worker.publisher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
@RequiredArgsConstructor
public class DailyTaskPublisher {
    private final TaskPublisher taskPublisher;

    @Scheduled(cron = "@daily")
    public void publishDailyTasks() {
//        taskPublisher.publishMenuBurgerDownloadTask();
        taskPublisher.publishFirstCategoryPageDataDownloadTask("/catalog/knigi-i-kantstovary/kantstovary/karty-i-globusy");
    }
}
