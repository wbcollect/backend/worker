package com.wbcollect.worker.config;

import com.wbcollect.common.GrpcAutoConfiguration;
import com.wbcollect.common.KafkaCustomAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ImportAutoConfiguration({
        KafkaCustomAutoConfiguration.class,
        GrpcAutoConfiguration.class
})
public class WorkerConfiguration {
}
