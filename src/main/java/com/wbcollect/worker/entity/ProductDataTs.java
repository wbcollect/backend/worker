package com.wbcollect.worker.entity;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(ProductDataTsId.class)
@Table(name = "product_data_ts")
public class ProductDataTs {
    @Id
    @Column(name = "product_id", nullable=false)
    private Long productId;

    @Id
    @Column(name = "taken_at", nullable=false)
    private Date takenAt;

    @Column
    private Integer price;

    @Column(name = "price_with_sale")
    private Integer priceWithSale;

    @Column
    private Integer quantity;
}
