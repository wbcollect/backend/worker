package com.wbcollect.worker.entity;

import javax.persistence.*;
import lombok.*;


@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_data")
public class ProductData {
    @Id
    @Column(name = "product_id")
    private Long productId;

    @Column
    private String name;

    @Column(name = "category_url")
    private String categoryUrl;

    @Column(name = "brand_id")
    private Long brandId;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "feedback_count")
    private Integer feedbackCount;

    @Column
    private Integer rating;

}
